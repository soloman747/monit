SYSTEM REQUIREMENTS

Memory and Disk space. A minimum of 1 megabytes RAM are required and around 500KB of free disk space. You may need more RAM depending on how many services Monit should monitor.

ANSI-C Compiler and Build System. You will need an ANSI-C99 compiler installed to build Monit. The GNU C compiler (GCC) from the Free Software Foundation (FSF) is recommended. In addition, your PATH must contain basic build tools such as make.

INSTALLATION

Monit utilize the GNU auto-tools and provided the requirements above are satisfied, building Monit is conducted via the standard;

# ./configure
# make
# make install

This will install Monit and the Monit man-file in /usr/local/bin and /usr/local/man/man1 respectively. If you want another location than /usr/local, run configure with the --prefix options and specify the install directory.

Use ./configure --help for build and install options. By default, Monit is built with SSL, PAM and large file support. You can change this with the --without-<xxx> options to ./configure. For instance, --without-ssl, --without-pam or --without-largefiles.
